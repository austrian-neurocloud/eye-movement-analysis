#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 13 10:55:17 2021

Creates a dictionary with the areas of interest (aoi). This is a necessary
first step in any analysis of eye movement data. It covers the most basic 
experimental setup, that is, when single  sentences are presented on one
line. It requires a text file (encoded in UTF8) which contains the sentences.
Umlaute (ä, ö, ü) and the ß must be replaced with alternative characters.
The punctuations characters may not be included.

The (command line) input arguments are: name of the stimulus file, the width
of a single character in pixels, and the leftmost x-coordinte (start) of the
stimulus.

@author: steha, matheusz
"""

import argparse
import pickle as pkl

parser = argparse.ArgumentParser(
        description = "Converts stimulus sentences to areas of interests.")
parser.add_argument(
    dest = "stim_file_path",
    help = "Path to stimulus sentence file.")
parser.add_argument(
    dest = "char_width",
    help = "The width of a single character in pixels.", type = int)
parser.add_argument(
    dest = "start_coor",
    help = "The leftmost x-coordinate (i.e., start) of the stimulus.",
    type = int)

args = parser.parse_args()

# read sentence stimulus file into a list
sen = []
with open(args.stim_file_path, 'r') as sen_file:
    for line in sen_file:
        # split sentence into words by space
        sen.append(line.rstrip())

# get the length (number of letters) for each word in the sentence       
n_chars = {}
for i, s in enumerate(sen, start=1): 
    n_chars['S' + str(i)] = [len(wrd) for wrd in s.split(' ')]
    
# create area of interest dictionary 
sen_aoi = {}
for i, key in enumerate(n_chars.keys(), start=1):
    aoi = [] # will be a list of tuples (x_start, x_end) of the aoi's
    start = args.start_coor - args.char_width * 0.5
    for chars in n_chars[key]:
        end = start + (chars + 1) * args.char_width
        aoi.append((start, end))
        # the next start is the former end 
        start =+  end
    sen_aoi['S' + str(i)] = aoi
    
with open('./aoi.pkl', 'wb') as pkl_file:
    pkl.dump(sen_aoi, pkl_file) 