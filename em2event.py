#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  4 14:30:35 2022

Converts eye link data file )(.asc) to an event-file (.json)
As yet, it only works for monocular recordings. With regard to 
saccades, the script converts to x and y coordinates to integer values
and ignores the amplitude and the velocity information. 

@author: steha
"""

from os.path import isdir
from os import mkdir 
import re
import numpy as np
import json

asc_dir = './ASCII/'
ascfile = 'pw03.asc'

start, stop = 'STIMON_', 'MASKON'
has_resp, resp, where = True, 'RESP', -1

with open(asc_dir + ascfile, 'r') as asc:
    raw = asc.readlines()
# empty dict to collect the events per trial
data, stimon = {}, False
for line in raw:
    if re.search(start, line):
        stimon = True
        stimcode = re.sub(start, '', line.split()[-1])
        event = {
                'fix'   : [],
                'sac'   : [],
                'blink' : False,
                'resp'  : None
                }
    if stimon:
        if re.search('EFIX', line):
            fix = line.split()[2:]
            fix = [x if x.isdecimal() else x[:-2] for x in fix]
            fix = np.array(fix).astype('int').tolist()
            event['fix'].append(fix)
        elif re.search('ESACC', line):
            sac = line.split()[2:-2]
            sac = [x if x.isdecimal() else x[:-2] for x in sac]
            sac = np.array(sac).astype('int').tolist()
            event['sac'].append(sac)
        elif re.search('EBLINK', line):
            event['blink'] = True
    if re.search(stop, line):    
        stimon = False
    if has_resp:
        if re.search(resp, line):
            event['resp'] = int(line.rstrip()[where])
    if re.search('TRIAL_RESULT', line):
        data[stimcode] = event
        
if not isdir('./JSON'):
    mkdir('./JSON')

# save it in a json-file
outfile = re.sub('asc', 'json', ascfile)
with open('./JSON/' + outfile, 'w') as jsonfile:
    json.dump(data, jsonfile, indent=2)    